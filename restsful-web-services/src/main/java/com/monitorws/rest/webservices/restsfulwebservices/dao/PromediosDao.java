package com.monitorws.rest.webservices.restsfulwebservices.dao;

import java.util.List;

import com.monitorws.rest.webservices.restsfulwebservices.dto.promedios.PromediosNokDTO;

public interface PromediosDao {
	
	public List<PromediosNokDTO> findPromediosNok( String entorno );
	public void testQuery();
}
