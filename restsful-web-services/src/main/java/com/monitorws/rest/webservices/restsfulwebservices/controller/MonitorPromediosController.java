package com.monitorws.rest.webservices.restsfulwebservices.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.monitorws.rest.webservices.restsfulwebservices.dto.promedios.PromediosNokDTO;
import com.monitorws.rest.webservices.restsfulwebservices.exception.DataNotFoundException;
import com.monitorws.rest.webservices.restsfulwebservices.service.PromediosService;

@RestController
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/monitorPromedios")
public class MonitorPromediosController {
	
	@Autowired
	private PromediosService promediosService;
	
	private static final Logger logger = LogManager.getLogger(MonitorPromediosController.class);
	
	@CrossOrigin
    @RequestMapping(value = "/PromediosNok", method = RequestMethod.GET)
    public List<PromediosNokDTO> retrieveOkAverage( @RequestParam("entorno") String entornoURL ){
		List<PromediosNokDTO> promediosOk = null;
		
    	logger.info("Inicia -> Servicio: MonitorPromedios, Método: retrieveOkAverage(), Entorno: "+ entornoURL);
    	promediosOk = promediosService.findPromediosNok( entornoURL );
    	
    	if(promediosOk == null) {
    		throw new DataNotFoundException("Entorno: "+entornoURL);
    	}
    	
    	return promediosOk;
    }
	
	@CrossOrigin
    @RequestMapping(value = "/TestQuery", method = RequestMethod.GET)
    public void retrieveTestQuery(){
    	logger.info("Inicia -> Servicio: MonitorPromedios, Método: retrieveOkAverage()");
    	
    	promediosService.testQuery();

    }
}
