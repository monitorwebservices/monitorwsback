package com.monitorws.rest.webservices.restsfulwebservices.mapper.graficasCash;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10TiemposRespuestaDTO;

public class Top10TiemposRespuestaMapper implements RowMapper<Top10TiemposRespuestaDTO>{
	
	public Top10TiemposRespuestaMapper() {	}

	@Override
	public Top10TiemposRespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		Top10TiemposRespuestaDTO objeto = new Top10TiemposRespuestaDTO();
		
		objeto.setServicio(rs.getString(1));
		objeto.setResponsable(rs.getString(2));
		objeto.setTiempo_respuesta(rs.getInt(3));
		
		return objeto;
	}
}