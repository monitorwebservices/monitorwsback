package com.monitorws.rest.webservices.restsfulwebservices.mapper.graficasGeneral;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.TiemposRespuestaCanalDTO;

public class TiemposRespuestaCanalDTOMapper implements RowMapper<TiemposRespuestaCanalDTO>{
	
	public TiemposRespuestaCanalDTOMapper(){
		
	}
	
	@Override
	public TiemposRespuestaCanalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		TiemposRespuestaCanalDTO objeto = new TiemposRespuestaCanalDTO();

		objeto.setCanal(rs.getString(1));
		objeto.setInterfaz(rs.getString(2));
		objeto.setServicio(rs.getString(3));
		objeto.setTiempoRespuesta(rs.getInt(4));
		
		return objeto;

	}

}
