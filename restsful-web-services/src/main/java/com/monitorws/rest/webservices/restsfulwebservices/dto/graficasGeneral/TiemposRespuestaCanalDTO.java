package com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral;

public class TiemposRespuestaCanalDTO {
	
	public TiemposRespuestaCanalDTO() {}
	
	String canal;
	String interfaz;
	String servicio;
	Integer tiempoRespuesta;
	
	public TiemposRespuestaCanalDTO(String canal, String interfaz, String servicio, Integer tiempoRespuesta) {
		super();
		this.canal = canal;
		this.interfaz = interfaz;
		this.servicio = servicio;
		this.tiempoRespuesta = tiempoRespuesta;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getInterfaz() {
		return interfaz;
	}

	public void setInterfaz(String interfaz) {
		this.interfaz = interfaz;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public Integer getTiempoRespuesta() {
		return tiempoRespuesta;
	}

	public void setTiempoRespuesta(Integer tiempoRespuesta) {
		this.tiempoRespuesta = tiempoRespuesta;
	}

	@Override
	public String toString() {
		return "TiemposRespuestaCanalDTO [canal=" + canal + ", interfaz=" + interfaz + ", servicio=" + servicio
				+ ", tiempoRespuesta=" + tiempoRespuesta + "]";
	}
}
