package com.monitorws.rest.webservices.restsfulwebservices.utilidades;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.jdbc.DataSourceBuilder;

public final class ObtenerEntorno  {

	private ObtenerEntorno() {}
	
	private static final Logger logger = LogManager.getLogger(ObtenerEntorno.class);

	Properties prop=new Properties(); // This class is available in java

	public static DataSource getPropertiesValues(String entorno) throws IOException{
		logger.info("Inicia -> DataSource assignation: getPropertiesValues");
		
		FileReader reader=new FileReader("src/main/java/config.properties");
		Properties p=new Properties();
	    p.load(reader);

	    return DataSourceBuilder
				.create()
				.username(p.getProperty(entorno +".username"))
				.password(p.getProperty(entorno +".password"))
				.url(p.getProperty(entorno +".url"))
				.driverClassName(p.getProperty(entorno +"driver-class-name"))
				.build();
	}
	
	public static String getEsquema(String entorno) throws IOException {
		
		FileReader reader=new FileReader("src/main/java/config.properties");
		Properties p=new Properties();
	    p.load(reader);
	    
		return p.getProperty(entorno +".schema");
	}
	
	public String setQuery(String consulta, String entorno) throws IOException {
		
		return null;
	}
}