package com.monitorws.rest.webservices.restsfulwebservices.dto.promedios;

public class PromediosNokDTO {

	String  aplicativo_Responsable = "";
	String  interfaz = "";
	String  servicio = "";
	String  metodo = "";
	Integer total_invocaciones;
	Integer total_ejcuciones_nok;
	double  tiempo_maximo;
	double  tiempo_minimo;
	double  tiempo_promedio;
	
	public PromediosNokDTO() {}

	public PromediosNokDTO(String aplicativo_Responsable, String interfaz, String servicio, String metodo,
			Integer total_invocaciones, Integer total_ejcuciones_nok, double tiempo_maximo, double tiempo_minimo,
			double tiempo_promedio) {
		super();
		this.aplicativo_Responsable = aplicativo_Responsable;
		this.interfaz = interfaz;
		this.servicio = servicio;
		this.metodo = metodo;
		this.total_invocaciones = total_invocaciones;
		this.total_ejcuciones_nok = total_ejcuciones_nok;
		this.tiempo_maximo = tiempo_maximo;
		this.tiempo_minimo = tiempo_minimo;
		this.tiempo_promedio = tiempo_promedio;
	}

	public String getAplicativo_Responsable() {
		return aplicativo_Responsable;
	}

	public void setAplicativo_Responsable(String aplicativo_Responsable) {
		this.aplicativo_Responsable = aplicativo_Responsable;
	}

	public String getInterfaz() {
		return interfaz;
	}

	public void setInterfaz(String interfaz) {
		this.interfaz = interfaz;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public Integer getTotal_invocaciones() {
		return total_invocaciones;
	}

	public void setTotal_invocaciones(Integer total_invocaciones) {
		this.total_invocaciones = total_invocaciones;
	}

	public Integer getTotal_ejcuciones_nok() {
		return total_ejcuciones_nok;
	}

	public void setTotal_ejcuciones_nok(Integer total_ejcuciones_nok) {
		this.total_ejcuciones_nok = total_ejcuciones_nok;
	}

	public double getTiempo_maximo() {
		return tiempo_maximo;
	}

	public void setTiempo_maximo(double tiempo_maximo) {
		this.tiempo_maximo = tiempo_maximo;
	}

	public double getTiempo_minimo() {
		return tiempo_minimo;
	}

	public void setTiempo_minimo(double tiempo_minimo) {
		this.tiempo_minimo = tiempo_minimo;
	}

	public double getTiempo_promedio() {
		return tiempo_promedio;
	}

	public void setTiempo_promedio(double tiempo_promedio) {
		this.tiempo_promedio = tiempo_promedio;
	}

	@Override
	public String toString() {
		return "PromediosOkDTO [aplicativo_Responsable=" + aplicativo_Responsable + ", interfaz=" + interfaz
				+ ", servicio=" + servicio + ", metodo=" + metodo + ", total_invocaciones=" + total_invocaciones
				+ ", total_ejcuciones_nok=" + total_ejcuciones_nok + ", tiempo_maximo=" + tiempo_maximo
				+ ", tiempo_minimo=" + tiempo_minimo + ", tiempo_promedio=" + tiempo_promedio + "]";
	}
}