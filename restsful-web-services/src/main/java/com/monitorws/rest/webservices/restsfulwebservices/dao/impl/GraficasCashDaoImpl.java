package com.monitorws.rest.webservices.restsfulwebservices.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.monitorws.rest.webservices.restsfulwebservices.dao.GraficasCashDao;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10ConsumosDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10TiemposRespuestaDTO;
import com.monitorws.rest.webservices.restsfulwebservices.utilidades.ObtenerEntorno;

@Repository
public class GraficasCashDaoImpl implements GraficasCashDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private static final Logger logger = LogManager.getLogger(GraficasCashDaoImpl.class);
	private static String fecha = "'29/01/19'";

	@Override
	public List<Top10ConsumosDTO> getTop10Consumos(String entorno) {
		
		logger.info("Inicia -> GraficasCashImpl : getTop10Consumos()");
		
		String esquema = null;
		DataSource dataSource = null;
		List<Top10ConsumosDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);

			String query = 
					"SELECT A1.BTISRVNOM, B1.JSBS200RP, COUNT (B1.JSBS200RP) AS TOTAL "
					+ " FROM " + esquema + ".BTI002 A1, " + esquema + ".JSBS200 B1"
					+ " WHERE A1.BTICANNOM = B1.JSBS200CN"
					+ " AND A1.BTINOM = B1.JSBS200IT " 
					+ " AND A1.BTISRVNOM = B1.JSBS200SR "
					+ " AND A1.BTIMTDNOM = B1.JSBS200MT "
					+ " AND A1.BTIFEC =" + fecha //SYSDATE " 
					+ " AND B1.JSBS200TP = 'C' "
					+ " AND B1.JSBS200RP <> 'N/A' "
					+ " AND A1.BTIEST = 2 "
					+ " GROUP BY A1.BTISRVNOM, B1.JSBS200RP "
					+ " ORDER BY 3 DESC "
					+ " FETCH FIRST 10 ROWS ONLY ";

			lista = jdbcTemplate.query(query, new RowMapper<Top10ConsumosDTO>() {
				@Override
				public Top10ConsumosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Top10ConsumosDTO(rs.getString("BTISRVNOM"), rs.getString("JSBS200RP"), rs.getInt("TOTAL"));
				}
			});
		}
		return lista;
	}
	
	@Override
	public List<Top10TiemposRespuestaDTO> getTop10TiemposRespuesta( String entorno ) {
		
		logger.info("Inicia -> GraficasCashImpl : getTop10TiemposRespuesta()");
		
		String esquema = null;
		DataSource dataSource = null;
		List<Top10TiemposRespuestaDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);
			
			String query = 
					" SELECT DISTINCT A1.BTISRVNOM, B1.JSBS200RP, A1.BTITIMSRV "
					+ " FROM " + esquema + ".BTI002 A1, " + esquema + ".JSBS200 B1"
					+ " WHERE A1.BTICANNOM = B1.JSBS200CN"
					+ " AND A1.BTINOM = B1.JSBS200IT"
					+ " AND A1.BTITIMSRV = (SELECT MAX(A2.BTITIMSRV) " 
										+ " FROM " + esquema + ".BTI002 A2 "
										+ " WHERE A2.BTISRVNOM = A1.BTISRVNOM ) "
					+ " AND A1.BTIFEC = " + fecha
					+ " AND A1.BTIMTDNOM = B1.JSBS200MT "
					+ " AND B1.JSBS200TP = 'C' "  
					+ " ORDER BY A1.BTITIMSRV DESC " 
					+ " FETCH FIRST 10 ROWS ONLY ";

			lista = jdbcTemplate.query(query, new RowMapper<Top10TiemposRespuestaDTO>() {
				@Override
				public Top10TiemposRespuestaDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Top10TiemposRespuestaDTO(rs.getString(1), rs.getString(2), rs.getInt(3));
				}
			});
		}
		return lista;
	}
	
	@Override
	public List<Top10ConsumosDTO> getTop10Fallas( String entorno ) {
		
		logger.info("Inicia -> GraficasCashImpl : getTop10Fallas()");

		String esquema = null;
		DataSource dataSource = null;
		List<Top10ConsumosDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);
			
			String query = 
					" SELECT A1.BTISRVNOM, B1.JSBS200RP, COUNT (B1.JSBS200RP) " 
					+ " FROM " + esquema + ".BTI002 A1, " + esquema + ".JSBS200 B1"
					+ " WHERE A1.BTICANNOM = B1.JSBS200CN"
					+ " AND A1.BTISRVNOM = B1.JSBS200SR "
					+ " AND A1.BTIMTDNOM = B1.JSBS200MT "
					+ " AND A1.BTIFEC = " + fecha //SYSDATE "
					+ " AND B1.JSBS200TP = 'C' "
					+ " AND BTIEST <> 2 "
					+ " GROUP BY A1.BTISRVNOM, B1.JSBS200RP "
					+ " ORDER BY 3 DESC "
					+ " FETCH FIRST 10 ROWS ONLY";
			
			lista = jdbcTemplate.query(query, new RowMapper<Top10ConsumosDTO>() {
				@Override
				public Top10ConsumosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Top10ConsumosDTO(rs.getString(1), rs.getString(2), rs.getInt(3));
				}
			});
		}
		return lista;
	}
}