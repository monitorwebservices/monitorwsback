package com.monitorws.rest.webservices.restsfulwebservices.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10ConsumosDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10TiemposRespuestaDTO;
import com.monitorws.rest.webservices.restsfulwebservices.service.GraficasCashService;

@RestController
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/monitorCash")
public class MonitorServiciosCashController {

	@Autowired
	private GraficasCashService monitorServiciosCash;

	private static final Logger logger = LogManager.getLogger(MonitorServiciosCashController.class);
	
	@CrossOrigin
    @RequestMapping(value = "/Consumos", method = RequestMethod.GET)
    public List<Top10ConsumosDTO> retrieveTop10Consumption( @RequestParam("entorno") String entornoURL ){
		
		logger.info("Inicia -> Servicio: MonitorServiciosCash, Método: retrieveTop10Consumos(), Entorno: " + entornoURL);
    	List<Top10ConsumosDTO> top_10_Consumos = monitorServiciosCash.getTop10Consumos(entornoURL);
    	
    	return top_10_Consumos;
    }
	
	@CrossOrigin
    @RequestMapping(value = "/TiemposRespuesta", method = RequestMethod.GET)
    public List<Top10TiemposRespuestaDTO> retrieveTop10ResponseTimes( @RequestParam("entorno") String entornoURL ){
    	
		logger.info("Inicia -> Servicio: MonitorServiciosCash, Método: retrieveTop10ResponseTimes(), Entorno: " + entornoURL);
    	List<Top10TiemposRespuestaDTO> top_10_Tiempos_Respuesta= monitorServiciosCash.getTop10TiemposRespuesta(entornoURL);
    	
    	return top_10_Tiempos_Respuesta;
    }
	
	@CrossOrigin
    @RequestMapping(value = "/Fallas", method = RequestMethod.GET)
    public List<Top10ConsumosDTO> retrieveTop10Errors( @RequestParam("entorno") String entornoURL ){
    	logger.info("Inicia -> Servicio: MonitorServiciosCash, Método: retrieveTop10Errors(), Entrno: " + entornoURL);
    	List<Top10ConsumosDTO> top_10_Consumos = monitorServiciosCash.getTop10Fallas(entornoURL);
    	
    	return top_10_Consumos;
    }
	
}
