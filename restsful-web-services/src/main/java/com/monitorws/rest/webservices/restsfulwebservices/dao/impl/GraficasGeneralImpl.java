package com.monitorws.rest.webservices.restsfulwebservices.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.monitorws.rest.webservices.restsfulwebservices.dao.GraficasGeneralDao;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.ErroresPorCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.TiemposRespuestaCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.Top10ErroresPorAplicativoDTO;
import com.monitorws.rest.webservices.restsfulwebservices.utilidades.ObtenerEntorno;

@Repository
public class GraficasGeneralImpl implements GraficasGeneralDao{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	private static final Logger logger = LogManager.getLogger(GraficasGeneralImpl.class);
	
	private static String fecha = "'29/01/19'";
	
	public List<Top10ErroresPorAplicativoDTO> findErroresPorAplicativo( String entorno ){
		
		logger.info("Inicia -> MonitorServiciosGeneralImpl : findErroresPorAplicativo()");
		
		String esquema = null;
		DataSource dataSource = null;
		List<Top10ErroresPorAplicativoDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);
			
			String query = 
					" SELECT A1.BTISRVNOM, A1.BTIMTDNOM, B1.JSBS200RP, COUNT(A1.BTISRVNOM) " 
					+ " FROM " + esquema + ".BTI002 A1, " + esquema + ".JSBS200 B1"
					+ " WHERE A1.BTISRVNOM = B1.JSBS200SR "
					+ " AND A1.BTIMTDNOM = B1.JSBS200MT "
					+ " AND A1.BTIEST <> 2 "
					+ " AND B1.JSBS200TP = 'G' "
					+ " GROUP BY A1.BTISRVNOM, A1.BTIMTDNOM, B1.JSBS200RP "
					+ " ORDER BY 4 DESC "
					+ " FETCH FIRST 10 ROWS ONLY"; 
			
			lista = jdbcTemplate.query(query, new RowMapper<Top10ErroresPorAplicativoDTO>() {
				@Override
				public Top10ErroresPorAplicativoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new Top10ErroresPorAplicativoDTO(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getInt(4));
				}
			});
		}
		return lista;
	}

	public List<ErroresPorCanalDTO> findErroresPorCanal( String entorno){
		
		logger.info("Inicia -> MonitorServiciosGeneralImpl : findErroresPorCanal()");

		String esquema = null;
		DataSource dataSource = null;
		List<ErroresPorCanalDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);
			
			String query=
					" SELECT BTICANNOM, COUNT (BTICANNOM) " 
					+ " FROM " + esquema + ".BTI002 "
					+ " WHERE BTIFEC = " + fecha //SYSDATE 
					+ " AND BTIEST <> 2 "
					+ " GROUP BY BTICANNOM "
					+ " FETCH FIRST 10 ROWS ONLY";
			
			lista = jdbcTemplate.query(query, new RowMapper<ErroresPorCanalDTO>() {
				@Override
				public ErroresPorCanalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new ErroresPorCanalDTO(
							rs.getString(1),
							rs.getInt(2));
				}
			});
		}
		return lista;
	}

	public List<TiemposRespuestaCanalDTO> findTiemposRespuestaCanal( String entorno ) {
		
		logger.info("Inicia -> MonitorServiciosGeneralImpl : findTiemposRespuestaCanal()");
		
		String esquema = null;
		DataSource dataSource = null;
		List<TiemposRespuestaCanalDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);
			
			String query = 
					" SELECT BTICANNOM, BTINOM, BTISRVNOM, BTITIMSRV " 
					+ " FROM " + esquema + ".BTI002 " 
					+ " WHERE BTIFEC = "+ fecha //SYSDATE
					+ " AND BTITIMSRV <> 0 " 
					+ " ORDER BY 4 DESC "
					+ " FETCH FIRST 10 ROWS ONLY ";
			
			lista = jdbcTemplate.query(query, new RowMapper<TiemposRespuestaCanalDTO>() {
				@Override
				public TiemposRespuestaCanalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new TiemposRespuestaCanalDTO(
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getInt(4));						
				}
			});
		}
		return lista;
	}
}
