package com.monitorws.rest.webservices.restsfulwebservices.dto.configuration;

public class ConfiguractionDTO {
	
	public ConfiguractionDTO() {
	}
	
	String url;
	String username;
	String password;
	String driver;
	String schema;
	
	public ConfiguractionDTO(String url, String username, String password, String driver, String schema) {
		super();
		this.url = url;
		this.username = username;
		this.password = password;
		this.driver = driver;
		this.schema = schema;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	@Override
	public String toString() {
		return "ConfiguractionDTO [url=" + url + ", username=" + username + ", password=" + password + ", driver="
				+ driver + ", schema=" + schema + "]";
	}

}
