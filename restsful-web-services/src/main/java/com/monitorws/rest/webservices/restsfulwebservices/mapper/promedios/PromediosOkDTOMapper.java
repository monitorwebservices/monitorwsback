package com.monitorws.rest.webservices.restsfulwebservices.mapper.promedios;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.monitorws.rest.webservices.restsfulwebservices.dto.promedios.PromediosNokDTO;

public class PromediosOkDTOMapper implements RowMapper<PromediosNokDTO>{
	
	public PromediosOkDTOMapper () {}

	@Override
	public PromediosNokDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		PromediosNokDTO objeto = new PromediosNokDTO();
		
		objeto.setAplicativo_Responsable(rs.getString(1));
		objeto.setInterfaz(rs.getString(2)); 
		objeto.setServicio(rs.getString(3));
		objeto.setMetodo(rs.getString(4));	
		objeto.setTotal_invocaciones(rs.getInt(5));
		objeto.setTotal_ejcuciones_nok(rs.getInt(6));
		objeto.setTiempo_maximo(rs.getDouble(7));
		objeto.setTiempo_minimo(rs.getDouble(8));
		objeto.setTiempo_promedio(rs.getDouble(9));

		return objeto;
	}

}
