package com.monitorws.rest.webservices.restsfulwebservices.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.monitorws.rest.webservices.restsfulwebservices.dao.PromediosDao;
import com.monitorws.rest.webservices.restsfulwebservices.dto.promedios.PromediosNokDTO;
import com.monitorws.rest.webservices.restsfulwebservices.utilidades.ObtenerEntorno;

@Repository
public class PromediosImpl implements PromediosDao{

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	private static final Logger logger = LogManager.getLogger(PromediosImpl.class);
	
	private static String fecha = "'29/01/19'";

	public List<PromediosNokDTO> findPromediosNok( String entorno ){
		
		logger.info("Inicia -> MonitorPromediosImpl : findPromediosOk()");
		
		String esquema = null;
		DataSource dataSource = null;
		List<PromediosNokDTO> lista =null;
		
		try {
			dataSource = ObtenerEntorno.getPropertiesValues(entorno);
			esquema = ObtenerEntorno.getEsquema(entorno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (dataSource != null) {
			jdbcTemplate.setDataSource(dataSource);
			
			String query =
					" SELECT B1.JSBS200RP, A1.BTINOM, A1.BTISRVNOM, A1.BTIMTDNOM, " 
					+ " COUNT (1) AS TOTAL, "
				    + " COUNT (CASE "
				                + " WHEN A1.BTIEST <> 2 THEN 1 " 
				                + " ELSE NULL "
				            + " END) AS NOK, "
				    + " TRUNC (MAX (A1.BTITIMSRV) / 1000,2), "
				    + " TRUNC (MIN (A1.BTITIMSRV) / 1000,2), "
				    + " TRUNC (AVG (A1.BTITIMSRV) / 1000,2) "
				+ " FROM " + esquema + ".BTI002 A1 "
				+ " INNER JOIN " + esquema + ".JSBS200 B1 "
				+ " ON A1.BTIFEC = " + fecha //SYSDATE
				+ " GROUP BY B1.JSBS200RP, A1.BTINOM, A1.BTISRVNOM, A1.BTIMTDNOM "
				+ " ORDER BY NOK DESC "
				+ " FETCH FIRST 10 ROWS ONLY ";
			
			lista = jdbcTemplate.query(query, new RowMapper<PromediosNokDTO>() {
				@Override
				public PromediosNokDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
					return new PromediosNokDTO(
							
							rs.getString(1),
							rs.getString(2),
							rs.getString(3),
							rs.getString(4),
							
							rs.getInt(5),
							rs.getInt(6),
							
							rs.getDouble(7),
							rs.getDouble(8),
							rs.getDouble(9));
				}
			});
		}
		return lista;
	}

	@Override
	public void testQuery() {
		// TODO Auto-generated method stub
		
	}
}