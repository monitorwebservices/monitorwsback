package com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash;

public class Top10ConsumosDTO {

	public Top10ConsumosDTO() {}
	
	String aplicativo;
	String servicio;
	Integer total;

	public Top10ConsumosDTO(String aplicativo, String servicio, Integer total) {
		this.aplicativo = aplicativo;
		this.servicio = servicio;
		this.total = total;
	}

	public String getAplicativo() {
		return aplicativo;
	}

	public void setAplicativo(String aplicativo) {
		this.aplicativo = aplicativo;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Top10Consumos [aplicativo=" + aplicativo + ", servicio=" + servicio + ", total=" + total + "]";
	}

}
