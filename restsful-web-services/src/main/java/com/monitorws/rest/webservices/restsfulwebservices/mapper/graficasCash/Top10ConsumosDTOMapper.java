package com.monitorws.rest.webservices.restsfulwebservices.mapper.graficasCash;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10ConsumosDTO;

public class Top10ConsumosDTOMapper implements RowMapper<Top10ConsumosDTO>{

	public Top10ConsumosDTOMapper() {	}

	@Override
	public Top10ConsumosDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		Top10ConsumosDTO objeto = new Top10ConsumosDTO();
		
		objeto.setAplicativo(rs.getString(1));
		objeto.setServicio(rs.getString(2));
		objeto.setTotal(rs.getInt(3));
		
		return objeto;
	}

}
