package com.monitorws.rest.webservices.restsfulwebservices.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.monitorws.rest.webservices.restsfulwebservices.dao.GraficasGeneralDao;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.ErroresPorCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.TiemposRespuestaCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.Top10ErroresPorAplicativoDTO;
import com.monitorws.rest.webservices.restsfulwebservices.service.GraficasGeneralService;

@Service
public class GraficasGeneralServiceImpl implements GraficasGeneralService{
	
	@Autowired
	private GraficasGeneralDao graficasGeneralDao;
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<Top10ErroresPorAplicativoDTO> findErroresPorAplicativo(String entorno){
		return graficasGeneralDao.findErroresPorAplicativo(entorno);
	}
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<ErroresPorCanalDTO> findErroresPorCanal(String entorno){
		return graficasGeneralDao.findErroresPorCanal(entorno);
	}
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<TiemposRespuestaCanalDTO> findTiemposRespuestaCanal(String entorno) {
		return graficasGeneralDao.findTiemposRespuestaCanal(entorno);
	}

}
