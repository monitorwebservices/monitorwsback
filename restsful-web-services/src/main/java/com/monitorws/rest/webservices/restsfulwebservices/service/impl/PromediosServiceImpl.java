package com.monitorws.rest.webservices.restsfulwebservices.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.monitorws.rest.webservices.restsfulwebservices.dao.PromediosDao;
import com.monitorws.rest.webservices.restsfulwebservices.dto.promedios.PromediosNokDTO;
import com.monitorws.rest.webservices.restsfulwebservices.service.PromediosService;

@Service
public class PromediosServiceImpl implements PromediosService{
	
	@Autowired
	private PromediosDao promediosDao;
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<PromediosNokDTO> findPromediosNok(String entorno){
		return promediosDao.findPromediosNok(entorno);
	}
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public void testQuery() {
		promediosDao.testQuery();
	}

}
