package com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral;

public class Top10ErroresPorAplicativoDTO {
	
	public Top10ErroresPorAplicativoDTO() {}
	
	String servicio; 
	String metodo; 
	String responsable; 
	Integer total;
	
	public Top10ErroresPorAplicativoDTO(String servicio, String metodo, String responsable, Integer total) {
		super();
		this.servicio = servicio;
		this.metodo = metodo;
		this.responsable = responsable;
		this.total = total;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getMetodo() {
		return metodo;
	}

	public void setMetodo(String metodo) {
		this.metodo = metodo;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "Top10ErroresPorAplicativoDTO [servicio=" + servicio + ", metodo=" + metodo + ", responsable="
				+ responsable + ", total=" + total + "]";
	}
}