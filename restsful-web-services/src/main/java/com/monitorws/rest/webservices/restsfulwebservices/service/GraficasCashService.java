package com.monitorws.rest.webservices.restsfulwebservices.service;

import java.util.List;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10ConsumosDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10TiemposRespuestaDTO;

public interface GraficasCashService {
	public List<Top10ConsumosDTO> getTop10Consumos( String entorno );
	public List<Top10TiemposRespuestaDTO> getTop10TiemposRespuesta( String entorno );
	public List<Top10ConsumosDTO> getTop10Fallas( String entorno );
}
