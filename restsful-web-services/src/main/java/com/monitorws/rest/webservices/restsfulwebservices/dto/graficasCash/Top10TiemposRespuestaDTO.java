package com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash;

public class Top10TiemposRespuestaDTO {
	
	public Top10TiemposRespuestaDTO() {}
	
	String servicio;
	String responsable;
	Integer tiempo_respuesta;
	
	public Top10TiemposRespuestaDTO(String servicio, String responsable, Integer tiempo_respuesta) {
		super();
		this.servicio = servicio;
		this.responsable = responsable;
		this.tiempo_respuesta = tiempo_respuesta;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public Integer getTiempo_respuesta() {
		return tiempo_respuesta;
	}

	public void setTiempo_respuesta(Integer tiempo_respuesta) {
		this.tiempo_respuesta = tiempo_respuesta;
	}

	@Override
	public String toString() {
		return "Top10TiemposRespuestaDTO [servicio=" + servicio + ", responsable=" + responsable + ", tiempo_respuesta="
				+ tiempo_respuesta + "]";
	}
	
}
