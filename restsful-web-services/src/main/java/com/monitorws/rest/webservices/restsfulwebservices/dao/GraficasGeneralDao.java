package com.monitorws.rest.webservices.restsfulwebservices.dao;

import java.util.List;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.ErroresPorCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.TiemposRespuestaCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.Top10ErroresPorAplicativoDTO;

public interface GraficasGeneralDao {
	
	public List<Top10ErroresPorAplicativoDTO> findErroresPorAplicativo( String entorno );
	public List<ErroresPorCanalDTO> findErroresPorCanal( String entorno );
    public List<TiemposRespuestaCanalDTO> findTiemposRespuestaCanal( String entorno );
}
