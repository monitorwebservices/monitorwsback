package com.monitorws.rest.webservices.restsfulwebservices.service;

import java.util.List;

import com.monitorws.rest.webservices.restsfulwebservices.dto.promedios.PromediosNokDTO;

public interface PromediosService {
	public List<PromediosNokDTO> findPromediosNok( String entorno );
	public void testQuery();
}
