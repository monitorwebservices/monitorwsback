package com.monitorws.rest.webservices.restsfulwebservices.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.ErroresPorCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.TiemposRespuestaCanalDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.Top10ErroresPorAplicativoDTO;
import com.monitorws.rest.webservices.restsfulwebservices.service.GraficasGeneralService;


@RestController
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@RequestMapping("/monitorServiciosGeneral")
public class MonitorServiciosGeneralController {
	
	@Autowired
	private GraficasGeneralService graficasGeneralService;
	
    private static final Logger logger = LogManager.getLogger(MonitorServiciosGeneralController.class);
    
    @CrossOrigin
    @RequestMapping(value = "/ErroresPorAplicativo", method = RequestMethod.GET)
    public List<Top10ErroresPorAplicativoDTO> retrieveApplicationErrors(@RequestParam("entorno") String entornoURL ){
    	
    	logger.info("Inicia -> Servicio: MonitorServiciosGeneralController, Método: retrieveApplicationErrors(), Entorno: " + entornoURL);
    	List<Top10ErroresPorAplicativoDTO> erroresAplicativo = graficasGeneralService.findErroresPorAplicativo(entornoURL);
    	return erroresAplicativo;
    }
    
    @CrossOrigin
    @RequestMapping(value = "/ErroresPorCanal", method = RequestMethod.GET)
    public List<ErroresPorCanalDTO> retrieveChannelErrors(@RequestParam("entorno") String entornoURL ){
    	
    	logger.info("Inicia -> Servicio: MonitorServiciosGeneralController, Método: retrieveChannelErrors(), Entorno: " + entornoURL);
    	List<ErroresPorCanalDTO> erroresCanal = graficasGeneralService.findErroresPorCanal(entornoURL);
    	return erroresCanal;
    }
    
    @CrossOrigin
    @RequestMapping(value = "/TiemposRespuestaCanal", method = RequestMethod.GET)
    public List<TiemposRespuestaCanalDTO> retrieveChannelTimeResponse(@RequestParam("entorno") String entornoURL ){

    	logger.info("Inicia -> Servicio: MonitorServiciosGeneralController, Método: retrieveChannelTimeResponse(), Entorno: " + entornoURL);
    	List<TiemposRespuestaCanalDTO> tiemposRespuesta= graficasGeneralService.findTiemposRespuestaCanal(entornoURL);
    	return tiemposRespuesta;
    }
}
