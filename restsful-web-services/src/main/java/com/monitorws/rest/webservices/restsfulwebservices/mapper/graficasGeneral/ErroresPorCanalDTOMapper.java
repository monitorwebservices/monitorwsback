package com.monitorws.rest.webservices.restsfulwebservices.mapper.graficasGeneral;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.ErroresPorCanalDTO;

public class ErroresPorCanalDTOMapper implements RowMapper<ErroresPorCanalDTO>{

	public ErroresPorCanalDTOMapper () {
		
	}
	
	@Override
	public ErroresPorCanalDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		ErroresPorCanalDTO objeto = new ErroresPorCanalDTO();
		
		objeto.setCanal(rs.getString(1));
		objeto.setTotal(rs.getInt(2)); 

		return objeto;
	}
}
