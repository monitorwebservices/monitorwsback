package com.monitorws.rest.webservices.restsfulwebservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SpringBootApplication
public class RestsfulWebServicesApplication {

    private static final Logger logger = LogManager.getLogger(RestsfulWebServicesApplication.class);
    
	public static void main(String[] args) {
		logger.info("Inicia monitoreo de WebServices");
		SpringApplication.run(RestsfulWebServicesApplication.class, args);
	}
}