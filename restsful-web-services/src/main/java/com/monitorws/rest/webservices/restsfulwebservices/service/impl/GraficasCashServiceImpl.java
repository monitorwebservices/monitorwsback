package com.monitorws.rest.webservices.restsfulwebservices.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.monitorws.rest.webservices.restsfulwebservices.dao.GraficasCashDao;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10ConsumosDTO;
import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasCash.Top10TiemposRespuestaDTO;
import com.monitorws.rest.webservices.restsfulwebservices.service.GraficasCashService;

@Service
public class GraficasCashServiceImpl implements GraficasCashService{
	
	@Autowired
	private GraficasCashDao graficasCashDao;
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<Top10ConsumosDTO> getTop10Consumos(String entorno){
		return graficasCashDao.getTop10Consumos(entorno);
	}
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<Top10TiemposRespuestaDTO> getTop10TiemposRespuesta(String entorno){
		return graficasCashDao.getTop10TiemposRespuesta(entorno);
	}
	
	@Transactional(readOnly = true, propagation= Propagation.REQUIRES_NEW)
	@Override
	public List<Top10ConsumosDTO> getTop10Fallas(String entorno){
		return graficasCashDao.getTop10Fallas(entorno);
	}

}
