package com.monitorws.rest.webservices.restsfulwebservices.mapper.graficasGeneral;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral.Top10ErroresPorAplicativoDTO;

public class Top10ErroresPorApliactivoDTOMapper implements RowMapper<Top10ErroresPorAplicativoDTO>{
	
	public Top10ErroresPorApliactivoDTOMapper() {	}
	
	@Override
	public Top10ErroresPorAplicativoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Top10ErroresPorAplicativoDTO objeto = new Top10ErroresPorAplicativoDTO();
		
		objeto.setServicio(rs.getString(1));
		objeto.setMetodo(rs.getString(2));
		objeto.setResponsable(rs.getString(3));
		objeto.setTotal(rs.getInt(4));
		
		return objeto;
	}

}
