package com.monitorws.rest.webservices.restsfulwebservices.dto.graficasGeneral;

public class ErroresPorCanalDTO {
	
	public ErroresPorCanalDTO() {}
	
	String canal; 
	Integer total;
	
	public ErroresPorCanalDTO(String canal, Integer total) {
		super();
		this.canal = canal;
		this.total = total;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "ErroresPorCanalDTO [canal=" + canal + ", total=" + total + "]";
	}

}
